# ordo

A python cli application that classifies text into `spam` or `ham` (not spam) and how confident the classifier is (`1.0` very confident, `0.0`: not confident).

## Installation and usage

```
python3 setup.py install
```

Once installed, use `ordo` from the cli:

```
ordo -t 'Lets go for lunch at Maccas bro...'
ordo -t 'Cheap Viagra for sale! SMS 111 for Samples!!'
```

Which returns:

```
Text: 'Lets go for lunch at Maccas bro...'
Prediction: [ham (1.0)]

Text: 'Cheap Viagra for sale! SMS 111 for Samples!!'
Prediction: [spam (0.3091909885406494)]
```

## Development

Setup a virtual env:

```
python3 -m venv env
source env/bin/activate # OSX.
.\env\Scripts\activate  # Windows.
```

Install in editable mode:

```
pip install -r requirements.txt
pip install -r dev_requirements.txt  # Dev requirements.
```

## Training

Training the text classifier can be invoked using the cli with `papermill`:

```
papermill train.ipynb train.ipynb -p epochs 10
```

Or just fire up `train.ipynb` using `jupyter notebook`.
