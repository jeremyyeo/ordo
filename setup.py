from setuptools import setup

setup(
    name="ordo",
    version="0.1",
    description="Python text classifier.",
    url="https://gitlab.com/jeremyyeo/ordo",
    packages=["src"],
    package_data={"src": ["model/best-model.pt"]},
    include_package_data=True,
    install_requires=["flair", "torch==1.2.0", "pandas", "notebook", "papermill"],
    entry_points={"console_scripts": ["ordo = src.__main__:main"]},
)
