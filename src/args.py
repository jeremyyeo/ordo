import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "-t",
    "--text",
    help="Pass the text that you want to classify in quotations. For example: ordo -t 'this is my text for classification'",
    type=str,
    dest="text",
    default=None,
)
args = parser.parse_args()
