from .args import args, parser
from .models import predict
import os


def main():
    if args.text == None or args == None:
        parser.print_help()
    else:
        input_text = args.text
        result = predict(input_text)
        print(f"Text: '{input_text}'\nPrediction: {result}")


if __name__ == "__main__":
    main()
