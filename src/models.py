from flair.models import TextClassifier
from flair.data import Sentence
import pkg_resources

# from pathlib import Path
# from os import path

# model_path = str(Path(__file__).resolve().parents[0]) + "/model/best-model.pt"
model_path = pkg_resources.resource_filename(__name__, "model/best-model.pt")


def predict(text):
    classifier = TextClassifier.load(model_path)
    sentence = Sentence(text)
    classifier.predict(sentence)
    return sentence.labels
